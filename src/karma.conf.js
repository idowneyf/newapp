// Karma configuration file, see link for more information
// https://karma-runner.github.io/1.0/config/configuration-file.html

module.exports = function(config) {
        config.set({
            basePath: '',
            frameworks: ['jasmine', '@angular-devkit/build-angular'],
            plugins: [
                require('karma-jasmine'),
                require('karma-jasmine-html-reporter'),
                require('karma-coverage-istanbul-reporter'),
                require('@angular-devkit/build-angular/plugins/karma'),
                require('karma-phantomjs-launcher'),
                require('karma-junit-reporter'),
                require('karma-remap-istanbul'),
                require('@angular-devkit/build-angular/plugins/karma')
                //require('@angular-cli/plugins/karma')
            ],

            preprocessors: {
                './src/test.ts': ['@angular-devkit/build-angular']
            },

            client: {
                clearContext: true // leave Jasmine Spec Runner output visible in browser
            },
            coverageIstanbulReporter: {
                dir: require('path').join(__dirname, '../coverage/nuevaapp'),
                reports: ['html', 'lcovonly', 'text-summary'],
                fixWebpackSourcePaths: true
            },
            reporters: ['dots', 'junit', 'karma-remap-istanbul'],
            junitReporter: {
                outputFile: 'test-results.xml'
            },
            //port: 9876,
            colors: true,
            logLevel: config.LOG_INFO,
            autoWatch: true,
            browsers: ['PhantomJS', 'PhantomJS_custom'],
            singleRun: true,
            restartOnFileChange: false,

            // define custom flags
            customLaunchers: {
                'PhantomJS_custom': {
                    base: 'PhantomJS',
                    options: {
                        windowName: 'my-window',
                        settings: {
                            webSecurityEnabled: false
                        },
                    },
                    flags: ['--load-images=true'],
                    debug: true
                }
            },

            phantomjsLauncher: {
                // Have phantomjs exit if a ResourceError is encountered (useful if karma exits without killing phantom)
                exitOnResourceError: false
            }
        });
    }
    //coments
    //reporters: ['dots', 'junit'],